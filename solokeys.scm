(define-module (solokeys)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cargo)
  #:use-module (guix licenses)
  #:use-module (gnu packages base)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages security-token))

(define-public rust-ansi-term-0.7
  (package
    (inherit rust-ansi-term-0.11)
    (name "rust-ansi-term")
    (version "0.7.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ansi_term" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "03gpxx3vf9mlc1q5xa31dpbvf1k07k9ggcz3vl31rv44mp85l9rh"))))
    (arguments
     `())
    (home-page "https://github.com/ogham/rust-ansi-term")
    (synopsis "A library for controlling colours and formatting")
    (description
     "This is a library for controlling colours and formatting, such as red bold text or blue underlined text, on ANSI terminals.")
    (license expat)))

(define-public rust-bitflags-0.4
  (package
    (inherit rust-bitflags-1)
    (name "rust-bitflags")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bitflags" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "18q4xvy9m7lhqwx46x1gzy5ywvpb6h4yb89ig5iwy9qi3i3dgsld"))))
    (home-page "https://github.com/bitflags/bitflags")
    (synopsis
     "Generates flags enums with well-defined semantics and ergonomic end-user APIs.")
    (description
     "Generates flags enums with well-defined semantics and ergonomic end-user APIs.
      You can use bitflags to:
      provide more user-friendly bindings to C APIs where flags may or may not be fully known in advance.
      generate efficient options types with string parsing and formatting support.")
    (license (list expat asl2.0))))

(define-public rust-strsim-0.4
  (package
    (inherit rust-strsim-0.9)
    (name "rust-strsim")
    (version "0.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "strsim" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "11g9rdaqh6yqhb28vcj8j5zdq1xrv9ij316bqjjk8rpdbifmfpqd"))))
    (home-page "https://github.com/dguo/strsim-rs")
    (synopsis "Rust implementations of string similarity metrics")
    (description
     "Rust implementations of string similarity metrics:

       Hamming
       Levenshtein - distance & normalized
       Optimal string alignment
       Damerau-Levenshtein - distance & normalized
       Jaro and Jaro-Winkler - this implementation of Jaro-Winkler does not limit the common prefix length
       Sørensen-Dice

     The normalized versions return values between 0.0 and 1.0, where 1.0 means an exact match.

     There are also generic versions of the functions for non-string inputs.")
    (license expat)))

(define-public rust-vec-map-0.4
  (package
    (inherit rust-vec-map-0.8)
    (name "rust-vec-map")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "vec_map" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "00lgjk07wdggvq0b21y78a4yijcyai5i7j6rx4kx8wkw1n51218q"))))
    (home-page "https://github.com/contain-rs/vec-map")
    (synopsis "A simple map based on a vector for small integer keys.")
    (description "A simple map based on a vector for small integer keys.")
    (license (list expat asl2.0))))

(define-public rust-clap-verbosity-flag-2.1
  (package
    (name "rust-clap-verbosity-flag")
    (version "2.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "clap-verbosity-flag" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1n5lpxkvv6yyaza8smizn9i0z4gbigqxvhx7xfm61r5bg5dj92d5"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-clap-builder" ,rust-clap-builder-4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ansi-term" ,rust-ansi-term-0.7)
                       ("rust-bitflags" ,rust-bitflags-0.4)
                       ("rust-strsim" ,rust-strsim-0.4)
                       ("rust-vec-map" ,rust-vec-map-0.4))))
    (home-page "https://github.com/clap-rs/clap-verbosity-flag")
    (synopsis "Easily add a `--verbose` flag to CLIs using Clap ")
    (description "Easily add a `--verbose` flag to CLIs using Clap ")
    (license (list expat asl2.0))))

(define-public rust-clap-verbosity-flag-2.0
  (package
    (name "rust-clap-verbosity-flag")
    (version "2.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "clap-verbosity-flag" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pp7zrchy9cgn0jz03v09jhp7n7sww9lcfqwxgr3sl89j1v0bvqy"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-clap-builder" ,rust-clap-builder-4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-ansi-term" ,rust-ansi-term-0.7)
                       ("rust-bitflags" ,rust-bitflags-0.4)
                       ("rust-strsim" ,rust-strsim-0.4)
                       ("rust-vec-map" ,rust-vec-map-0.4))))
    (home-page "https://github.com/clap-rs/clap-verbosity-flag")
    (synopsis "Easily add a `--verbose` flag to CLIs using Clap ")
    (description "Easily add a `--verbose` flag to CLIs using Clap ")
    (license (list expat asl2.0))))

(define-public rust-delog-0.1
  (package
    (name "rust-delog")
    (version "0.1.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "delog" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "13xckqdw8b4s47sfqmq9lm0vdl808n11n9a18jfhqd32h8v96axg"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-log" ,rust-log-0.4))))
    (home-page "https://delog.rs/")
    (synopsis
     "Deferred logging, an implementation and extension of Rust's standard logging facade.")
    (description
     "Deferred logging, an implementation and extension of Rust's standard logging facade.")
    (license (list expat asl2.0))))

(define-public rust-flexiber-0.1
  (package
    (name "rust-flexiber")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "flexiber" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1r9qg4bsdhl7p9hxn435lahhnnlpk65kin28b0zv1izfcqa5ppz3"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-delog" ,rust-delog-0.1))))
    (home-page "https://github.com/nickray/flexiber")
    (synopsis
     "Encoding and decoding of BER-TLV as described in ISO 7816-4, without allocations.")
    (description
     "Encoding and decoding of BER-TLV as described in ISO 7816-4, without allocations.
      Follows the approach taken in der, and then in simple-tlv.")
    (license (list expat asl2.0))))

(define-public rust-hidapi-2
  (package
    (name "rust-hidapi")
    (version "2.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hidapi" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ydgyvs1bilc50xn7x8m7jk0p563v1mljiy9mqq5gk0d7ck7fdvj"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-winapi" ,rust-winapi-0.3)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/ruabmbua/hidapi-rs")
    (synopsis "Rust-y wrapper around hidapi")
    (description
     "This crate provides a rust abstraction over the features of the C library hidapi. Based off of hidapi-rs by Osspial.")
    (license expat)))

(define-public rust-indicatif-0.17
  (package
    (inherit rust-indicatif-0.16)
    (name "rust-indicatif")
    (version "0.17.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "indicatif" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "098ggvg7ps4097p5n9hmb3pqqy10bi8vjfzb7pci79xrklf78a7v"))))
    (home-page "https://github.com/console-rs/indicatif")
    (synopsis "A progress bar and cli reporting library for Rust")
    (description
     "A Rust library for indicating progress in command line applications to users.
      This currently primarily provides progress bars and spinners as well as basic color support, but there are bigger plans for the future of this!")
    (license expat)))

(define-public rust-iso7816-0.1
  (package
    (name "rust-iso7816")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "iso7816" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lpnwfn63hvfwywhvv5q8agybyzwpf8cx5amhsvlk4sh7msarrp7"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-delog" ,rust-delog-0.1)
                       ("rust-heapless" ,rust-heapless-0.7))))
    (home-page "https://github.com/ycrypto/iso7816")
    (synopsis "Types for ISO 7816")
    (description "Types for ISO 7816")
    (license (list expat asl2.0))))

(define-public rust-enum-iterator-derive-1
  (package
    (name "rust-enum-iterator-derive")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "enum-iterator-derive" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1jzxgfhz4i0hvnw5qzqhwscky8vsmxljv89g0navisacay4qbkzf"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/stephaneyfx/enum-iterator.git")
    (synopsis "Procedural macro to derive Sequence.")
    (description "Procedural macro to derive Sequence")
    (license bsd-0)))

(define-public rust-enum-iterator-1
  (package
    (name "rust-enum-iterator")
    (version "1.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "enum-iterator" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12g63d7w5vpq964f7fbnahw2qiqsml3f9j3rxrk6f1yxnmrkipbs"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-enum-iterator-derive" ,rust-enum-iterator-derive-1))))
    (home-page "")
    (synopsis
     "Tools to iterate over all values of a type (e.g. all variants of an enumeration)")
    (description "Tools to iterate over the values of a type.")
    (license bsd-0)))

(define-public rust-pkcs11-0.5
  (package
    (name "rust-pkcs11")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pkcs11" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1bqhdd4w0ymvv6zzc090vy2mywq07lix16am8pz3nqf8wiknvjis"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-libloading" ,rust-libloading-0.5)
                       ("rust-num-bigint" ,rust-num-bigint-0.2))))
    (home-page "https://github.com/mheese/rust-pkcs11")
    (synopsis "Rust PKCS#11 Library")
    (description
     "This is a library which brings support for PKCS#11 to Rust. It is aiming at having both a very low-level API to map the PKCS#11 functionality to Rust as well as having a higher-level API for more easy usage as well as bringing more safety for programming against PKCS#11.")
    (license asl2.0)))

(define-public rust-uriparse-0.6
  (package
    (name "rust-uriparse")
    (version "0.6.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "uriparse" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1zsjwc715x4720y4b3dsdras50imvaakqgyl59n3j2fq0kyd0002"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-fnv" ,rust-fnv-1)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/sgodwincs/uriparse-rs")
    (synopsis "A URI parser including relative references")
    (description
     "Implementation of RFC3986 including URIs and URI references.")
    (license expat)))

(define-public rust-pkcs11-uri-0.1
  (package
    (name "rust-pkcs11-uri")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pkcs11-uri" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0nkl6l0r70cfdm1grlkwlk2g5s1sg6n80ris905np6rjmswqnvdr"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-pkcs11" ,rust-pkcs11-0.5)
                       ("rust-anyhow" ,rust-anyhow-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-uriparse" ,rust-uriparse-0.6))))
    (home-page "https://nickray.github.io/pkcs11-uri/pkcs11_uri/")
    (synopsis "PKCS #11 URI parser")
    (description "PKCS #11 URI parser")
    (license (list expat asl2.0))))

(define-public rust-const-oid-0.7
  (package
    (inherit rust-const-oid-0.9)
    (name "rust-const-oid")
    (version "0.7.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "const-oid" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1wwl3cncd8p2fa54vzmghflh4nh9ml02xfbv38nf5ziifh28riz4"))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/const-oid")
    (synopsis
     "Const-friendly implementation of the ISO/IEC Object Identifier (OID) standard as defined in ITU X.660, with support for BER/DER encoding/decoding as well as heapless no_std (i.e. embedded) support")
    (description
     "Const-friendly implementation of the ISO/IEC Object Identifier (OID) standard as defined in ITU X.660, with support for BER/DER encoding/decoding as well as heapless no_std (i.e. embedded) environments.")
    (license expat)))

(define-public rust-crypto-bigint-0.3
  (package
    (inherit rust-crypto-bigint-0.5)
    (name "rust-crypto-bigint")
    (version "0.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "crypto-bigint" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "08gx92sj93hk2smqy4nvk8lmpjjjqm7a9ps22q3pxqqxzbas3ih3"))))
    (home-page "https://github.com/RustCrypto/crypto-bigint")
    (synopsis
     "Pure Rust implementation of a big integer library which has been designed from the ground-up for use in cryptographic applications. Provides constant-time, no_std-friendly implementations of modern formulas using const generics.")
    (description
     "Pure Rust implementation of a big integer library which has been designed from the ground-up for use in cryptographic applications.
      Provides constant-time, no_std-friendly implementations of modern formulas using const generics.")
    (license (list expat asl2.0))))

(define-public rust-pem-rfc7468-0.3
  (package
    (inherit rust-pem-rfc7468-0.7)
    (name "rust-pem-rfc7468")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pem-rfc7468" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0c7vrrksg8fqzxb7q4clzl14f0qnqky7jqspjqi4pailiybmvph1"))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/pem-rfc7468")
    (synopsis
     "PEM Encoding (RFC 7468) for PKIX, PKCS, and CMS Structures, implementing a strict subset of the original Privacy-Enhanced Mail encoding.")
    (description
     " PEM Encoding (RFC 7468) for PKIX, PKCS, and CMS Structures, implementing a strict subset of the original Privacy-Enhanced Mail encoding intended specifically for use with cryptographic keys, certificates, and other messages. Provides a no_std-friendly, constant-time implementation suitable for use with cryptographic private keys.")
    (license (list expat asl2.0))))

(define-public rust-der-0.5
  (package
    (inherit rust-der-0.7)
    (name "rust-der")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "der" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0p3h7nszn7jhjacpmkjrcyx5g8p3ma1qhxfy3397m7l3fdfq26b9"))))
    (arguments
     (list
      #:cargo-inputs `(("rust-const-oid" ,rust-const-oid-0.7)
                       ("rust-crypto-bigint" ,rust-crypto-bigint-0.3)
                       ("rust-pem-rfc7468" ,rust-pem-rfc7468-0.3))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/der")
    (synopsis
     "Pure Rust embedded-friendly implementation of the Distinguished Encoding Rules (DER) for Abstract Syntax Notation One (ASN.1) as described in ITU X.690 with full support for heapless no_std targets")
    (description
     "Pure Rust embedded-friendly implementation of the Distinguished Encoding Rules (DER) for Abstract Syntax Notation One (ASN.1) as described in ITU X.690.")
    (license (list expat asl2.0))))

(define-public rust-pkcs1-0.3
  (package
    (inherit rust-pkcs1-0.7)
    (name "rust-pkcs1")
    (version "0.3.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pkcs1" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0813szfx13n4xl6l19m3lwj7pqgljqwc6ipxhr2dv0yc9k06d3x7"))))
    (arguments
     (list
      #:cargo-inputs `(("rust-der" ,rust-der-0.5))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/pkcs1")
    (synopsis
     "Pure Rust implementation of Public-Key Cryptography Standards (PKCS) #1: RSA Cryptography Specifications Version 2.2 (RFC 8017)")
    (description
     "Pure Rust implementation of Public-Key Cryptography Standards (PKCS) #1: RSA Cryptography Specifications Version 2.2 (RFC 8017).")
    (license (list expat asl2.0))))

(define-public rust-spki-0.5
  (package
    (inherit rust-spki-0.7)
    (name "rust-spki")
    (version "0.5.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "spki" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09qaddm4kw01xm9638910bm4yqnshzh2p38lvc3kxkvc5b01ml24"))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/spki")
    (synopsis
     "X.509 Subject Public Key Info (RFC5280) describing public keys as well as their associated AlgorithmIdentifiers (i.e. OIDs)")
    (description
     "X.509 Subject Public Key Info types describing public keys as well as their associated AlgorithmIdentifiers (i.e. OIDs). Specified in RFC 5280 § 4.1.")
    (license (list expat asl2.0))))

(define-public rust-pkcs8-0.8
  (package
    (inherit rust-pkcs8-0.10)
    (name "rust-pkcs8")
    (version "0.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pkcs8" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1l29h4mrgi2kpsl98jzky3ni5by3xa1sc6db9yd8l1i1p0zxmavw"))))
    (arguments
     (list
      #:cargo-inputs `(("rust-spki" ,rust-spki-0.5))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/pkcs8")
    (synopsis
     "Pure Rust implementation of Public-Key Cryptography Standards (PKCS) #8: Private-Key Information Syntax Specification (RFC 5208), with additional support for PKCS#8v2 asymmetric key packages (RFC 5958)")
    (description
     "Pure Rust implementation of Public-Key Cryptography Standards (PKCS) #8: Private-Key Information Syntax Specification (RFC 5208), with additional support for PKCS#8v2 asymmetric key packages (RFC 5958)")
    (license (list expat asl2.0))))

(define-public rust-rsa-0.6
  (package
    (inherit rust-rsa-0.9)
    (name "rust-rsa")
    (version "0.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rsa" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "02viiiylxpk2hx5h5qrpm4lcd8ildvafbw0rn6rx44wnqia2gwjc"))))
    (arguments
     (list
      #:cargo-inputs `(("rust-pkcs1" ,rust-pkcs1-0.3)
                       ("rust-num-bigint-dig" ,rust-num-bigint-dig-0.8)
                       ("rust-pkcs8" ,rust-pkcs8-0.8))))
    (home-page "https://github.com/RustCrypto/RSA")
    (synopsis "Pure Rust RSA implementation")
    (description "A portable RSA implementation in pure Rust.")
    (license (list expat asl2.0))))

(define-public rust-rfc6979-0.3
  (package
    (inherit rust-rfc6979-0.4)
    (name "rust-rfc6979")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rfc6979" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1fzsp705b5lhwd2r9il9grc3lj6rm3b2r89vh0xv181gy5xg2hvp"))))
    (home-page "https://github.com/RustCrypto/signatures/tree/master/rfc6979")
    (synopsis
     "Pure Rust implementation of RFC6979: Deterministic Usage of the Digital Signature Algorithm (DSA) and Elliptic Curve Digital Signature Algorithm (ECDSA)")
    (description
     "Pure Rust implementation of RFC6979: Deterministic Usage of the Digital Signature Algorithm (DSA) and Elliptic Curve Digital Signature Algorithm (ECDSA)")
    (license (list expat asl2.0))))

(define-public rust-signature-2.0
  (package
    (inherit rust-signature-2)
    (name "rust-signature")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "signature" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0zg534qaa8cl5spq8d0rs0jq6km4w9vil69148awiy9khg4mir4g"))))
    (home-page "https://github.com/RustCrypto/traits/tree/master/signature")
    (synopsis
     "Traits for cryptographic signature algorithms (e.g. ECDSA, Ed25519)")
    (description
     "This crate contains traits which provide generic, object-safe APIs for generating and verifying digital signatures.
      Used by the dsa, ecdsa, ed25519, and rsa crates maintained by the RustCrypto organization, as well as ed25519-dalek.")
    (license (list expat asl2.0))))

(define-public rust-ecdsa-0.15
  (package
    (inherit rust-ecdsa-0.16)
    (name "rust-ecdsa")
    (version "0.15.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ecdsa" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0zk3nz2qlczayd8w7zp3nh1skxh5nvrk1l16m62l3msab50l310j"))))
    (arguments
     (list
      #:cargo-inputs `(("rust-rfc6979" ,rust-rfc6979-0.3)
                       ("rust-signature" ,rust-signature-2.0))))
    (home-page "https://github.com/RustCrypto/signatures/tree/master/ecdsa")
    (synopsis
     "Pure Rust implementation of the Elliptic Curve Digital Signature Algorithm (ECDSA) as specified in FIPS 186-4 (Digital Signature Standard), providing RFC6979 deterministic signatures as well as support for added entropy.")
    (description
     "Elliptic Curve Digital Signature Algorithm (ECDSA) as specified in FIPS 186-4 (Digital Signature Standard).")
    (license (list expat asl2.0))))

(define-public rust-lpc55-0.2
  (package
    (name "rust-lpc55")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lpc55" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1idixv6hfbrpivhl27cbnzjqmivphknzjgim3ivcr3gjqkmzpv7j"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-delog" ,rust-delog-0.1)
                       ("rust-aes" ,rust-aes-0.8)
                       ("rust-anyhow" ,rust-anyhow-1)
                       ("rust-atty" ,rust-atty-0.2)
                       ("rust-base64" ,rust-base64-0.21)
                       ("rust-bitflags" ,rust-bitflags-1.2)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-ctr" ,rust-ctr-0.9)
                       ("rust-delog" ,rust-delog-0.1)
                       ("rust-enum-iterator" ,rust-enum-iterator-1)
                       ("rust-pem" ,rust-pem-1)
                       ("rust-pkcs11" ,rust-pkcs11-0.5)
                       ("rust-pkcs11-uri" ,rust-pkcs11-uri-0.1)
                       ("rust-rsa" ,rust-rsa-0.6)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-signature" ,rust-signature-1)
                       ("rust-tiny-http" ,rust-tiny-http-0.12)
                       ("rust-ecdsa" ,rust-ecdsa-0.15))))
    (home-page "https://github.com/lpc55/lpc55-host")
    (synopsis
     "Host-side tooling to interact with LPC55 chips via the ROM bootloader")
    (description
     "Host-side tooling to interact with LPC55 chips via the ROM bootloader")
    (license (list expat asl2.0))))

(define-public rust-base16ct-0.1
  (package
    (inherit rust-base16ct-0.2)
    (name "rust-base16ct")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "base16ct" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1klccxr7igf73wpi0x3asjd8n0xjg0v6a7vxgvfk5ybvgh1hd6il"))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/base16ct")
    (synopsis
     "Pure Rust implementation of Base16 a.k.a hexadecimal (RFC 4648) which avoids any usages of data-dependent branches/LUTs and thereby provides portable \"best effort\" constant-time operation and embedded-friendly no_std support")
    (description
     "Pure Rust implementation of Base16 (RFC 4648).
Implements lower and upper case Base16 variants without data-dependent branches or lookup tables, thereby providing portable \"best effort\" constant-time operation.
Supports no_std environments and avoids heap allocations in the core API but also provides optional alloc support for convenience.")
    (license (list expat asl2.0))))

(define-public rust-crypto-bigint-0.4
  (package
    (inherit rust-crypto-bigint-0.5)
    (name "rust-crypto-bigint")
    (version "0.4.9")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "crypto-bigint" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1vqprgj0aj1340w186zyspi58397ih78jsc0iydvhs6zrlilnazg"))))
    (home-page "https://github.com/RustCrypto/crypto-bigint")
    (synopsis
     "Pure Rust implementation of a big integer library which has been designed from the ground-up for use in cryptographic applications. Provides constant-time, no_std-friendly implementations of modern formulas using const generics.")
    (description
     "Pure Rust implementation of a big integer library which has been designed from the ground-up for use in cryptographic applications.
Provides constant-time, no_std-friendly implementations of modern formulas using const generics.")
    (license (list expat asl2.0))))

(define-public rust-ff-0.12
  (package
    (inherit rust-ff-0.13)
    (name "rust-ff")
    (version "0.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ff" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0q3imz4m3dj2cy182i20wa8kbclgj13ddfngqb2miicc6cjzq4yh"))))
    (home-page "https://github.com/zkcrypto/ff")
    (synopsis "Library for building and interfacing with finite fields.")
    (description
     "ff is a finite field library written in pure Rust, with no unsafe{} code.")
    (license (list expat asl2.0))))

(define-public rust-group-0.12
  (package
    (inherit rust-group-0.13)
    (name "rust-group")
    (version "0.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "group" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ixspxqdpq0hxg0hd9s6rngrp6rll21v4jjnr7ar1lzvdhxgpysx"))))
    (home-page "https://github.com/zkcrypto/group")
    (synopsis "Elliptic curve group traits and utilities")
    (description
     "group is a crate for working with groups over elliptic curves.")
    (license (list expat asl2.0))))

(define-public rust-sec1-0.3
  (package
    (inherit rust-sec1-0.7)
    (name "rust-sec1")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sec1" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0a09lk5w3nyggpyz54m10nnlg9v8qbh6kw3v1bgla31988c4rqiv"))))
    (home-page "https://github.com/RustCrypto/formats/tree/master/sec1")
    (synopsis
     "Pure Rust implementation of SEC1: Elliptic Curve Cryptography encoding formats including ASN.1 DER-serialized private keys as well as the Elliptic-Curve-Point-to-Octet-String encoding.")
    (description
     "Pure Rust implementation of SEC1: Elliptic Curve Cryptography encoding formats including ASN.1 DER-serialized private keys as well as the Elliptic-Curve-Point-to-Octet-String encoding.")
    (license (list expat asl2.0))))

(define-public rust-elliptic-curve-0.12
  (package
    (inherit rust-elliptic-curve-0.13)
    (name "rust-elliptic-curve")
    (version "0.12.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "elliptic-curve" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1lwi108mh6drw5nzqzlz7ighdba5qxdg5vmwwnw1j2ihnn58ifz7"))))
    (arguments
     `(#:cargo-inputs (("rust-base16ct" ,rust-base16ct-0.1)
                       ("rust-crypto-bigint" ,rust-crypto-bigint-0.4)
                       ("rust-ff" ,rust-ff-0.12)
                       ("rust-group" ,rust-group-0.12)
                       ("rust-sec1" ,rust-sec1-0.3))))
    (home-page
     "https://github.com/RustCrypto/traits/tree/master/elliptic-curve")
    (synopsis
     "General purpose Elliptic Curve Cryptography (ECC) support, including types and traits for representing various elliptic curve forms, scalars, points, and public/secret keys composed thereof.")
    (description
     "General purpose Elliptic Curve Cryptography (ECC) support, including types and traits for representing various elliptic curve forms, scalars, points, and public/secret keys composed thereof.")
    (license (list expat asl2.0))))

(define-public rust-primeorder-0.12
  (package
    (inherit rust-primeorder-0.13)
    (name "rust-primeorder")
    (version "0.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "primeorder" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1cn5lh5pb1g7x9l0cq888qp6im36bg95pkqlyji6bfix3c9zfm0b"))))
    (home-page
     "https://github.com/RustCrypto/elliptic-curves/tree/master/primeorder")
    (synopsis
     "Pure Rust implementation of complete addition formulas for prime order elliptic curves (Renes-Costello-Batina 2015). Generic over field elements and curve equation coefficients.")
    (description
     "Pure Rust implementation of complete addition formulas for prime order elliptic curves (Renes-Costello-Batina 2015). Generic over field elements and curve equation coefficients.")
    (license (list expat asl2.0))))

(define-public rust-p256-0.12
  (package
    (inherit rust-p256-0.13)
    (name "rust-p256")
    (version "0.12.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "p256" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0m8f1d0n69bvm4xpranhwv3nrvcq3lcfqn4cqsxbqhyfrfrj9ha9"))))
    (arguments
     `(#:cargo-inputs (("rust-elliptic-curve" ,rust-elliptic-curve-0.12)
                       ("rust-primeorder" ,rust-primeorder-0.12))))
    (home-page
     "https://github.com/RustCrypto/elliptic-curves/tree/master/p256")
    (synopsis
     "Pure Rust implementation of the NIST P-256 (a.k.a. secp256r1, prime256v1) elliptic curve with support for ECDH, ECDSA signing/verification, and general purpose curve arithmetic.")
    (description
     "Pure Rust implementation of the NIST P-256 (a.k.a. secp256r1, prime256v1) elliptic curve with support for ECDH, ECDSA signing/verification, and general purpose curve arithmetic support implemented in terms of traits from the elliptic-curve crate.")
    (license (list expat asl2.0))))

(define-public rust-pcsc-sys-1.2
  (package
    (name "rust-pcsc-sys")
    (version "1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pcsc-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1si37v9n07r3csqcnnqn4i82j75b6dssyz0fzdg1n3rcpbnbzdz1"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/bluetech/pcsc-rust")
    (synopsis "Low-level bindings to the PC/SC C API")
    (description "Low-level bindings to the PC/SC C API.")
    (license (list expat asl2.0))))

(define-public rust-pcsc-2.8
  (package
    (name "rust-pcsc")
    (version "2.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pcsc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1d5smcr9irdppl20r7wg2s5xqwfdxx5sb7q5v2l0is04knzb1jip"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-pcsc-sys" ,rust-pcsc-sys-1.2))))
    (home-page "https://delog.rs/")
    (synopsis "Bindings to the PC/SC API for smart card communication")
    (description "Bindings to the PC/SC API for smart card communication.")
    (license expat)))

(define-public rust-pcsc-2.4
  (package
    (name "rust-pcsc")
    (version "2.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pcsc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "10vcnmxrpscg0fcrzz0r1klgiqzgh2axvyg1zyqwk8h5hy6rmq48"))))
    (build-system cargo-build-system)
    (arguments
     (list
      #:cargo-inputs `(("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-pcsc-sys" ,rust-pcsc-sys-1.2))))
    (home-page "https://github.com/bluetech/pcsc-rust")
    (synopsis "Bindings to the PC/SC API for smart card communication.")
    (description "Rust bindings to the PC/SC API for smart card communication.
    Nice, safe API.
    Tested on Linux, Windows, macOS.
    Mostly zero overhead.")
    (license expat)))

(define-public rust-rcgen-0.10
  (package
    (inherit rust-rcgen-0.11)
    (name "rust-rcgen")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rcgen" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0nvqgr697xzdzaavkcwcl59kxw7xfx9zdhdzx49fm3gkwbpq9gpz"))))
    (home-page "https://github.com/est31/rcgen")
    (synopsis "Rust X.509 certificate generator")
    (description "Simple Rust library to generate X.509 certificates.")
    (license (list expat asl2.0))))

(define-public rust-x509-parser-0.14
  (package
    (inherit rust-x509-parser-0.15)
    (name "rust-x509-parser")
    (version "0.14.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "x509-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1j7b3xxpwik38y9rajglmhis551gj3zz5irw1vj1bqkwnsvvxv70"))))
    (home-page "https://github.com/rusticata/x509-parser.git")
    (synopsis "Parser for the X.509 v3 format (RFC 5280 certificates)")
    (description
     "A X.509 v3 (RFC5280) parser, implemented with the nom parser combinator framework.
It is written in pure Rust, fast, and makes extensive use of zero-copy. A lot of care is taken to ensure security and safety of this crate, including design (recursion limit, defensive programming), tests, and fuzzing. It also aims to be panic-free.")
    (license (list expat asl2.0))))

(define-public rust-hex-literal-0.3.1
  (package
    (inherit rust-hex-literal-0.3)
    (name "rust-hex-literal")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hex-literal" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1j0ri05s2r5870v4mj13j019x069w6zkdccjhgblbi8vxwszdwas"))))
    (home-page "https://github.com/RustCrypto/utils")
    (synopsis
     "Macro for converting hexadecimal string to a byte array at compile time.")
    (description
     "This crate provides the hex! macro for converting hexadecimal string literals to a byte array at compile time.")
    (license (list expat asl2.0))))

(define-public solo2-cli
  (package
    (name "solo2-cli")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/solokeys/" name
                           "/archive/refs/tags/v" version ".tar.gz"))
       (sha256
        ;; (base32 "0ck95limlnkva3vg3mcmnpa2y1n9dnka6d8njja852sa2zfzk7mf"))))
        (base32 "1d2z6glya1axnbr6p3p926b0jdmrz10rf210lmlfk2zkwrg0r8s9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-clap-complete" ,rust-clap-complete-4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-lazy-static" ,rust-lazy-static-1)
                       ("rust-clap-verbosity-flag" ,rust-clap-verbosity-flag-2.0)
                       ("rust-ctrlc" ,rust-ctrlc-3)
                       ("rust-dialoguer" ,rust-dialoguer-0.10)
                       ("rust-flexiber" ,rust-flexiber-0.1)
                       ("rust-hex" ,rust-hex-0.4)
                       ("rust-hex-literal" ,rust-hex-literal-0.3)
                       ("rust-hidapi" ,rust-hidapi-2)
                       ("rust-indicatif" ,rust-indicatif-0.17)
                       ("rust-iso7816" ,rust-iso7816-0.1)
                       ("rust-lpc55" ,rust-lpc55-0.2)
                       ("rust-p256" ,rust-p256-0.12)
                       ("rust-pcsc" ,rust-pcsc-2.8)
                       ("rust-pkcs8" ,rust-pkcs8-0.9)
                       ("rust-pretty-env-logger" ,rust-pretty-env-logger-0.4)
                       ("rust-rcgen" ,rust-rcgen-0.10)
                       ("rust-ureq" ,rust-ureq-2)
                       ("rust-webbrowser" ,rust-webbrowser-0.8)
                       ("rust-x509-parser-0.14" ,rust-x509-parser-0.14)
                       ("rust-sha1" ,rust-sha1-0.10)
                       ("rust-sha2" ,rust-sha2-0.10))
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'fix-sha-1-crate
                    ;; https://github.com/solokeys/solo2-cli/pull/106/commits/c7cfa7698c9cb9a78b8ea5f1039d096d994ecd60
                    (lambda _
                      ;; The sha-1 crate was depecrated.
                      (substitute* "Cargo.toml"
                        (("sha-1")
                         "sha1"))))
		  (add-after 'unpack 'fix-udev-rules
                    ;; https://github.com/solokeys/solo2-cli/issues/9#issuecomment-969197785
                    (lambda _
                      ;; There is a typo in the udev rules that must be fixed.
                      (substitute* "70-solo2.rules"
                        (("b000")
                         "beee"))))
                  (add-before 'install 'install-udev-rules
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                        (mkdir-p (string-append out "/lib/udev/rules.d"))
                        (copy-file "70-solo2.rules"
                                   (string-append out "/lib/udev/rules.d/"
                                                  "70-solo2.rules")) #t)))
		  (add-before 'install 'install-bash-completions
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                        (mkdir-p (string-append out "/share/bash-completion/completions"))
                        (copy-file "target/release/solo2.bash"
                                   (string-append out "/share/bash-completion/completions/"
                                                  "solo2.bash")) #t)))
		  )))
    (home-page "https://github.com/solokeys/solo2-cli")
    (native-inputs (list perl pkg-config))
    (inputs (list libevdev libgudev pcsc-lite))
    (synopsis "solo2 library and cli")
    (description "This provides the tools to manage solokeys 2 devices")
    (license (list expat asl2.0))))

solo2-cli
